<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\UsersInterface',
            'App\Services\UsersService'
        );

        $this->app->bind(
            'App\Contracts\DoctorsInterface',
            'App\Services\v1\DoctorsService'
        );

        $this->app->bind(
            'App\Contracts\v1\ContactsInterface',
            'App\Services\v1\ContactsService'
        );
        $this->app->bind(
        	'App\Contracts\v1\TimeslotsInterface',
	        'App\Services\v1\TimeslotsService'
        );

        $this->app->bind(
        	'App\Contracts\v1\AppointmentsInterface',
	        'App\Services\v1\AppointmentsService'
        );
    }
}

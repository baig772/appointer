<?php
/**
 * Created by PhpStorm.
 * User: baig772
 * Date: 3/1/18
 * Time: 3:19 PM
 */

namespace App\Services\v1;


use App\Contracts\DoctorsInterface;

use App\Doctor;
use App\FavoriteDoctors;
use App\Timeslots;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DoctorsService implements DoctorsInterface
{

    public function getAllWithSpecializations()
    {
        // TODO: Implement getAllWithSpecializations() method.

        $fav_doc    =   FavoriteDoctors::where('user_id', Auth::user()->id)->get()->pluck('doctor_id')->toArray();


        $sql    =   "SELECT  d.id, d.qualification,d.first_name,d.last_name,GROUP_CONCAT(s.name) AS `specializations` FROM doctors d
                      LEFT JOIN doctor_specialization ds ON d.id = ds.doctor_id
                      LEFT JOIN specializations s ON ds.specialization_id = s.id  
                      WHERE d.deleted_at IS NULL 
                      GROUP BY d.first_name, d.last_name, d.id
                      ";


        $data   =   DB::select(DB::raw($sql));



        foreach ($data  as  $datum) {
            $query  =   "SELECT c.name, ts.start_time, ts.end_time, ts.day, ts.shift
                        FROM
                        doctor_clinics dc
                        LEFT JOIN clinics c on dc.clinic_id = c.id
                        LEFT JOIN timeslots ts ON dc.id = ts.doctor_clinic_id
                        WHERE
                        dc.doctor_id = $datum->id";

            $details    =   DB::select(DB::raw($query));
            foreach ($details as $detail) {
                $detail->start_time    =   substr($detail->start_time, 0 , -3);
                $detail->end_time    =   substr($detail->end_time, 0 , -3);
            }
            $datum->details =   $details;

            if(in_array($datum->id, $fav_doc)) {
                $datum->is_favorite =   true;
            } else {
                $datum->is_favorite =   false;
            }

        }
        return $data;

    }

    public function favoriteDoctor($doctor_id, $action)
    {
        // TODO: Implement favoriteDoctor() method.

        $doctor =   Doctor::where('id', $doctor_id)->first();
        if(!$doctor) {
            return false;
        } else {
            if($action == 1) {
                $fav_doc    =   new FavoriteDoctors();
                $fav_doc->user_id    =   Auth::user()->id;
                $fav_doc->doctor_id =   $doctor_id;
                $fav_doc->save();
            } else if ($action == 0) {
                $fav_doc    =   FavoriteDoctors::where('user_id', Auth::user()->id)
                                ->where('doctor_id', $doctor_id)->get();

                foreach ($fav_doc as $item) {
                    $item->delete();
                }
            }
            return true;
        }

    }

    public function getFavoriteDoctors()
    {
        // TODO: Implement getFavoriteDoctors() method.
        $sql    =   "SELECT fd.user_id, fd.doctor_id, d.first_name, d.last_name, d. qualification ,GROUP_CONCAT(s.name) as 'specializations'
                      FROM favourite_doctors fd
                      LEFT JOIN doctors d on fd.doctor_id = d.id
                      LEFT JOIN doctor_specialization ds ON d.id = ds.doctor_id
                      LEFT JOIN specializations s ON ds.specialization_id = s.id
                      WHERE fd.user_id = ".Auth::user()->id."
                      AND fd.deleted_at is null
                      GROUP BY d.first_name, d.last_name, fd.doctor_id;";
        $result =   DB::select(DB::raw($sql));

        foreach ($result as $item) {
            $query  =   "SELECT c.name, ts.start_time, ts.end_time, ts.day, ts.shift
                        FROM
                        doctor_clinics dc
                        LEFT JOIN clinics c on dc.clinic_id = c.id
                        LEFT JOIN timeslots ts ON dc.id = ts.doctor_clinic_id
                        WHERE
                        dc.doctor_id = $item->doctor_id";

            $details    =   DB::select(DB::raw($query));

            foreach ($details as $detail) {
                $detail->start_time    =   substr($detail->start_time, 0 , -3);
                $detail->end_time    =   substr($detail->end_time, 0 , -3);
            }
            $item->details  =   $details;
            $item->is_favorite  =   true;
        }

        return $result;
    }

    public function searchDoctors($string)
    {
        // TODO: Implement searchDoctors() method.

        $doctors   =   Doctor::where('first_name', 'LIKE', '%'.$string.'%')
                        ->orWhere('last_name', 'LIKE', '%'.$string.'%')->get();
        return $doctors ? $doctors : false;
    }

    public function getDoctorDetailsById($id)
    {

    	// TODO: Implement getDoctorById() method.

	    $data   =   DB::table('doctor_clinics as dc')

                    ->select(
                    	    'c.name as clinic_name', 'c.id as clininc_id',
	                        'dc.id as dc_id',
                            'd.id as doctor_id', 'd.first_name as doctor_name'
	                        )
                    ->join('doctors as d', 'dc.doctor_id', '=', 'd.id')
                    ->join('clinics as c', 'dc.clinic_id', '=', 'c.id')
                    ->where('doctor_id', $id)
                    ->GroupBy('c.id')
                    ->get();
	    foreach ($data as $datum) {
	    	$datum->timeslots   =   Timeslots::where('doctor_clinic_id', $datum->dc_id)->get(['start_time', 'end_time', 'day']);
	    }


        return $data ? $data : false;
    }
}
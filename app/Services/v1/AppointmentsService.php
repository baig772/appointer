<?php
/**
 * Created by PhpStorm.
 * User: baig772
 * Date: 3/29/18
 * Time: 2:40 PM
 */

namespace App\Services\v1;


use App\Appointments;
use App\ContactList;
use App\Contracts\v1\AppointmentsInterface;
use App\Timeslots;
use App\TimeslotSessions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AppointmentsService implements AppointmentsInterface {

	public function bookAppointment($arguments) {
		$contact    =   null;
		$result     =   null;

		if($arguments['personal_appointment'] == 0) {
			$contact    =   ContactList::where('id', $arguments['contact_id'])->first();
		}

		if($arguments['contact_id'] == 0) {
			$arguments['contact_id']    =   null;
		}

		// TODO: Implement bookAppointment() method.
		$sessions    =   DB::table('timeslots as t')
						->leftJoin('timeslot_sessions as ts', 'ts.timeslot_id', '=', 't.id')
						->select(
						't.id as timeslot_id', 't.total_tokens as total_tokens',
						'ts.total_tokens_assigned'

						)
					->where('t.id', $arguments['timeslot_id'])
					->where('ts.session_date', $arguments['appointment_date'])
					->get();

		$total_sessions =   count($sessions);
		$timeslot = Timeslots::where('id', $arguments['timeslot_id'])->first();
		if($total_sessions === 0) {
			//create session
			$timeslot_session                           =   new TimeslotSessions();
			$timeslot_session->timeslot_id              =   $timeslot->id;
			$timeslot_session->timeslot_session         =   $timeslot->shift;
			$timeslot_session->session_date             =   $arguments['appointment_date'];
			$timeslot_session->total_tokens_assigned    =   1;

			$timeslot_session->save();


			$appointment            =   new Appointments();
			$appointment->user_id   =   Auth::user()->id;
			$appointment->doctor_id =   $arguments['doctor_id'];
			$appointment->timeslot_session_id   =   $timeslot_session->id;
			if($contact) {
				$appointment->patient_name  =   $contact['first_name']." ".$contact['last_name'];
				$appointment->phone_number  =   $contact['phone_number'];

			} else {
				$appointment->patient_name  =   Auth::user()->first_name." ".Auth::user()->last_name;
				$appointment->phone_number  =   Auth::user()->phone_number;
			}

			$appointment->contact_id    =   $arguments['contact_id'];
			$appointment->user_itself   =   $arguments['personal_appointment'];
			$appointment->token_number  =   1;

			$appointment->save();

			$result =   $appointment;




		} else {
			$timeslot_session   =   TimeslotSessions::where('timeslot_id', $arguments['timeslot_id'])
									->where('session_date', $arguments['appointment_date'])
									->first();
			$existing_appointments   =   Appointments::where('timeslot_session_id', $timeslot_session->id)->get();
			$existing_appointments   =   count($existing_appointments);
			$token                   =   $existing_appointments+1;

			$appointment    =   new Appointments();

			$appointment->user_id   =   Auth::user()->id;
			$appointment->doctor_id =   $arguments['doctor_id'];
			$appointment->timeslot_session_id   =   $timeslot_session->id;
			if($contact) {
				$appointment->patient_name  =   $contact['first_name']." ".$contact['last_name'];
				$appointment->phone_number  =   $contact['phone_number'];

			} else {
				$appointment->patient_name  =   Auth::user()->first_name." ".Auth::user()->last_name;
				$appointment->phone_number  =   Auth::user()->phone_number;
			}

			$appointment->contact_id    =   $arguments['contact_id'];
			$appointment->user_itself   =   $arguments['personal_appointment'];
			$appointment->token_number  =   $token;

			$appointment->save();

			$result =   $appointment;
		}

		return $result ? $result : false;

	}
}
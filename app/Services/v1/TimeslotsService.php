<?php
/**
 * Created by PhpStorm.
 * User: baig772
 * Date: 3/26/18
 * Time: 3:23 PM
 */

namespace App\Services\v1;


use App\Clinics;
use App\Contracts\v1\TimeslotsInterface;
use App\Doctor;
use App\Timeslots;

class TimeslotsService implements TimeslotsInterface {
	public function getTimeslots( $doc_id, $clinic_id, $dc_id ) {
		// TODO: Implement getTimeslots() method.
		$doctor  =   Doctor::where('id', $doc_id)->first();
		$doctor_detail  =   [
			'id'    =>  $doctor->id,
			'first_name'    =>  $doctor->first_name,
			'last_name'     =>  $doctor->last_name
		];

		$clinic =   Clinics::where('id', $clinic_id)->first()->toArray();

		$data   =   [
			'doctor'    =>  $doctor_detail
		];

		$slots  =   Timeslots::where('doctor_clinic_id', $dc_id)->get()->toArray();

		$result =   [];
		foreach ($slots as $slot) {
			$result[$slot['day']][] = $slot;
		}

		$data['clinic'] =   $clinic;
		$data['slots']  =   $result;

		return $data ? $data : false;
	}
}
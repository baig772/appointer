<?php
/**
 * Created by PhpStorm.
 * User: baig772
 * Date: 3/7/18
 * Time: 4:56 PM
 */

namespace App\Services\v1;


use App\ContactList;
use App\Contracts\v1\ContactsInterface;
use Illuminate\Support\Facades\Auth;

class ContactsService implements ContactsInterface
{
    public function getAll()
    {
        // TODO: Implement getAll() method.
        $contacts   =   ContactList::where('user_id', Auth::user()->id)->get();
        return $contacts ? $contacts : false;
    }

    public function addContact($data)
    {
        // TODO: Implement addContact() method.
        $contact    =   new ContactList();
        $name   =   $data['name'];
        $exploded   =   explode(' ', $name, 2);
        $first_name =   $exploded[0];
        if(isset($exploded[1])) {
            $last_name  =   $exploded[1];
        } else {
            $last_name  =   null;
        }

        $contact->first_name    =   $first_name;
        $contact->last_name     =   $last_name;
        $contact->phone_number  =   $data['phone_number'];
        $contact->user_id       =   Auth::user()->id;
        if($contact->save()) {
            return $contact->id;
        } else {
            return false;
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: baig772
 * Date: 2/16/18
 * Time: 5:24 PM
 */

namespace App\Services;


use App\Contracts\UsersInterface;
use App\User;
use App\Countries;

class UsersService implements UsersInterface
{
    private $user;
    function __construct()
    {
        $this->user =   new User();
    }

    public function verifyUser($input)
    {

        $phone_number   =   $input['country_code'].$input['phone_number'];

        $user   =  User::where('phone_number', $phone_number)->first();

        if(!$user) {
            return  $this->addUser($input);
        } else {
            return $this->getUserByPhone($phone_number);
        }
    }

    public function addUser($data)
    {
        //get the country from country code
        $country    =   Countries::where('phone_code', $data['country_code'])->first();
        //preparing user object to register for the first time
        $name           =   $data['name'];
        $phone_number   =   $data['country_code'].$data['phone_number'];
        $exploded_name  =   explode(' ', $name, 2);

        $first_name     =   $exploded_name[0];
        if(isset($exploded_name[1])) {
            $last_name      =   $exploded_name[1];
        }
        else {
            $last_name  =   null;
        }
        $secret_code    =   mt_rand(1111,9999);

        $location       =   $this->getLatLong($country->name);



        $user   =   new User();
        $user->first_name   =   $first_name;
        $user->last_name    =   $last_name;
        $user->phone_number =   $phone_number;
        $user->secret_code  =   $secret_code;
        $user->country_id   =   $country->id;
        $user->latitude     =   $location['lat'];
        $user->longitude    =   $location['lng'];
        $user->save();
        return $user;
    }

    public function verifyOtp($otp, $phone)
    {
        // TODO: Implement verifyOtp() method.
        $user   =   User::where('secret_code', $otp)
                            ->where('phone_number', $phone)->first();
        if($user) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserByPhone($phone)
    {
        // TODO: Implement getUserByPhone() method.
        $user   =   User::where('phone_number', $phone)->first();
        $user->secret_code  =   mt_rand(1111,9999);
        $user->save();
        return $user;
    }

    public function getLatLong($country) {
        $url        = "https://maps.googleapis.com/maps/api/geocode/json?key=".env('GMAP_API')."&address=$country";
        $json_data  =  file_get_contents($url);
        $result     =  json_decode($json_data);


        $location   =   false;
        $latitude   = $result->results[0]->geometry->location->lat;
        $longitude  = $result->results[0]->geometry->location->lng;


        $location   =   [
            'lat'   =>  $latitude,
            'lng'   =>  $longitude
        ];

        return $location;

    }

    public function resendOtp($phone)
    {
        // TODO: Implement resendOtp() method.
        $user   =   User::where('phone_number', $phone)->first();
        if($user) {
            if($user->secret_code) {
                return $user->secret_code;
            } else {
                $rand   =   $user->secret_code  =   mt_rand(1111,9999);
                $user->save();
                return $rand;
            }
        } else {
            return false;
        }

    }


}
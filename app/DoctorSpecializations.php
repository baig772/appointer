<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorSpecializations extends Model
{
    //
    protected $table    =   'doctor_specialization';
}

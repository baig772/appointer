<?php
/**
 * Created by PhpStorm.
 * User: baig772
 * Date: 3/7/18
 * Time: 4:56 PM
 */

namespace App\Contracts\v1;


interface ContactsInterface
{
    public function getAll();

    public function addContact($data);
}
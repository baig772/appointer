<?php
/**
 * Created by PhpStorm.
 * User: baig772
 * Date: 3/29/18
 * Time: 2:40 PM
 */

namespace App\Contracts\v1;


interface AppointmentsInterface {

	public function bookAppointment($args);
}
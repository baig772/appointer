<?php
/**
 * Created by PhpStorm.
 * User: baig772
 * Date: 3/26/18
 * Time: 3:22 PM
 */

namespace App\Contracts\v1;


interface TimeslotsInterface {

	public function getTimeslots($doc_id, $clinic_id, $dc_id);
}
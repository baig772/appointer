<?php
/**
 * Created by PhpStorm.
 * User: baig772
 * Date: 3/1/18
 * Time: 3:19 PM
 */

namespace App\Contracts;


interface DoctorsInterface
{
    public function getAllWithSpecializations();

    public function favoriteDoctor($doctor_id, $action);

    public function getFavoriteDoctors();

    public function searchDoctors($string);

    public function getDoctorDetailsById($id);
}
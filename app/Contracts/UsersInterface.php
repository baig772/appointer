<?php
/**
 * Created by PhpStorm.
 * User: baig772
 * Date: 2/16/18
 * Time: 5:19 PM
 */

namespace App\Contracts;


interface UsersInterface
{
    public function verifyUser($input);

    public function addUser($data);

    public function verifyOtp($otp, $phone);

    public function getUserByPhone($phone);

    public function resendOtp($phone);
}
<?php

namespace App\Http\Controllers\API\v1;

use App\Contracts\v1\ContactsInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
class ContactListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ContactsInterface $contacts)
    {
        if($request->wantsJson()) {
            $data   =   null;
            $contact_list   =   $contacts->getAll();
            if($contact_list) {
                $data   =   [
                    'error' =>  false,
                    'contacts'  =>  $contact_list
                ];
            } else {
                $data   =   [
                    'error' =>true,
                    'message'   =>  'No contacts found'
                ];
            }

            return response()->json($data, 200);
        } else {
            return response()->json(['error'    =>  'Forbidden'], 403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ContactsInterface $contacts)
    {
        if($request->wantsJson()) {
            $status =   200;
            $data   =   null;
            $rules  =   [
                'name'  =>  'required',
                'phone_number'  =>  'required'
            ];

            $validator  =   Validator::make($request->all(), $rules);
            if($validator->fails()) {
                $status    =   400;

                $data   =   [
                    'error' =>  true,
                    'messages'  =>  $validator->errors()
                ];

            } else {
                $contact    =   $contacts->addContact($request->all());
                if($contact) {
                    $data   =   [
                        'error'   =>  false,
                        'message' =>  'Contact added',
                        'contact_id'  =>  $contact
                    ];
                } else {
                    $status =   400;
                    $data   =   [
                        'error' =>  true,
                        'message'   =>  'Contact not added'
                    ];
                }
            }


            return response()->json($data, $status);
        } else {
            return response()->json(['error'    =>  'Forbidden'], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

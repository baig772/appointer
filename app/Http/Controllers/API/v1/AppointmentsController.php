<?php

namespace App\Http\Controllers\API\v1;

use App\Contracts\v1\AppointmentsInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppointmentsController extends Controller
{
    public function confirmAppointment(Request $request, AppointmentsInterface $appointments) {
		if($request->wantsJson()) {
			$response   =   null;
			$status =   200;
			$appointment    =   $appointments->bookAppointment($request->all());
			if($appointment) {
				$response   =   [
					'error' =>  false,
					'appointment'   =>  $appointment
				];
				$status =   201;
			} else {
				$status =   400;
				$response   =   [
					'error' =>  true,
					'messages'  =>  'There was an error booking your appointment'
				];
			}

			return response()->json($response, $status);
		} else {
			return response()->json(['error'    =>  'Forbidden'], 403);
		}

		
    }
}

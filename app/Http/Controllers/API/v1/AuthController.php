<?php

namespace App\Http\Controllers\API\v1;

use App\Contracts\UsersInterface;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{


    public function login(Request $request, UsersInterface $users) {
        if($request->wantsJson()) {
            $response   =   [];
            $rules  =   [
                'name'  =>  'required',
                'phone_number'  =>  'required',
                'country_code'  =>  'required'
            ];
            $validator  =   Validator::make($request->all(), $rules);
            if($validator->fails()) {
                $response   =   [
                    'error' =>  true,
                    'msg'   =>  $validator->errors()
                ];
                return response()->json($response, 400);
            } else {
                $user   =   $users->verifyUser($request->all());
                $response   =   [
                    'error' =>  false,
                    'secret_code'   =>  $user->secret_code." by Quaidtech",
                ];

                return response()->json($response);
            }

        } else {
            return response()->json(['error'    =>  'Unauthorized Access'], 403);
        }
    }

    public function verifyOtp(Request $request, UsersInterface $users) {
        if($request->wantsJson()) {
            $response   =   [];
            $rules  =   [
                'phone_number'  =>  'required',
                'otp'           =>  'required',

            ];

            $validator  =   Validator::make($request->all(), $rules);
            if($validator->fails()) {
                $response   =   [
                    'error' =>  true,
                    'msg'   =>  $validator->errors()
                ];

                return response()->json($response, 400);
            } else {
                $verify =   $users->verifyOtp($request['otp'], $request['phone_number']);
                if($verify) {
                    $status =   200;
                    $user   =   User::where('phone_number', $request['phone_number'])->first();
                    $user->status   =   1;
                    $user->secret_code  =   null;
                    $token  =   $user->createToken('API')->accessToken;

                    $user->save();

                    $response   =   [
                        'error' =>false,
                        'token' =>  $token
                    ];


                } else {
                    $response   =   [
                        'error'   =>  true,
                        'msg'   =>  'Unable to verify',
                    ];
                    $status =   400;
                }

                return response()->json($response, $status);
            }

        } else {
            return response()->json(['error'    =>  'Unauthorized Access'], 403);
        }
    }

    public function resendOtp(Request $request, UsersInterface $users) {
        if($request->wantsJson()) {
            //verify user by phone number
            $data   =   null;
            $user   =   $users->resendOtp($request->phone_number);
            if($user) {

                $data   =   [
                    'error' =>  false,
                    'secret_code'  => $user .' by Quaid Tech'
                ];
            } else {
                $data   =   [
                    'error' =>  true,
                    'message'   =>  'No user found'
                ];
            }

            return response()->json($data, 200);
        } else {
            return response()->json(['error'    =>  'Forbidden'], 403);
        }
    }
}

<?php

namespace App\Http\Controllers\API\v1;

use App\Contracts\DoctorsInterface;
use App\Doctor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoctorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, DoctorsInterface $doctors)
    {
        if($request->wantsJson()) {
            $response   =   [];
            $list    =   $doctors->getAllWithSpecializations();

            $response   =   [
                'error' =>  false,
                'doctors'   =>  $list
            ];
            return response()->json($response);
        } else {
            return response()->json(['error'    =>  'Unauthorized Access'], 403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request, DoctorsInterface $doctors)
    {
        if($request->wantsJson()) {

            $data   =   null;
            $status =   200;
            $doctor =   $doctors->getDoctorDetailsById($id);
            if($doctor) {
                $data   =   [
                    'error' =>  false,
                    'doctors'  =>   $doctor
                ];
            } else {
                $data   =   [
                    'error' =>true,
                    'message'   =>  'No data found'
                ];
                $status =   400;
            }

            return response()->json($data, $status);
        } else {
            return response()->json(['error'    =>  'Forbidden'], 403);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function favorite(Request $request, DoctorsInterface $doctors) {
        if($request->wantsJson()) {
            $status =   200;
            $data   =   [];
            if($doctors->favoriteDoctor($request->doctor_id, $request->is_fav)) {
                $data   =   [
                    'errors'    =>  false,
                    'message'   =>  trans('messages.greeting'),
                ];
            } else {
                $data   =   [
                    'error' =>  true,
                    'message'   =>  'No doctor found with the ID'
                ];
                $status =   400;
            }

            return response()->json($data, $status);
        } else {
            return response()->json(['error'    =>  'Forbidden'], 403);
        }
    }

    public function favoriteDoctors(Request $request, DoctorsInterface $doctors) {
        if($request->wantsJson()) {
            $data   =   [];
            $fav_doctors    =   $doctors->getFavoriteDoctors();
            $data   =   [
                'error' =>  false,
                'doctors'   =>  $fav_doctors
            ];

            return response()->json($data);
        } else {
            return response()->json(['error'   => 'Forbidden'], 403);
        }
    }

    public function search(Request $request, DoctorsInterface $doctors) {
        if($request->wantsJson()) {
            $data   =   null;
            $doctors    =   $doctors->searchDoctors($request->name);

            if($doctors) {
                $data   =   [
                    'error' =>  false,
                    'doctors'   =>  $doctors
                ];
            } else {
                $data   =   [
                    'error' =>  true,
                    'message'   =>  'No doctors found'
                ];
            }

            return response()->json($data, 200);
        } else {
            return response()->json(['error'    =>  'Forbidden'], 403);
        }
    }


}

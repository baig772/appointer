<?php

namespace App\Http\Controllers\API\v1;

use App\Contracts\v1\TimeslotsInterface;
use function GuzzleHttp\Psr7\parse_response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TimeslotsController extends Controller
{
    public function getTimeslots(Request $request, $doctor_id, $clinic_id, $dc_id, TimeslotsInterface $timeslots) {
    	if($request->wantsJson()) {
			$slots   =   $timeslots->getTimeslots($doctor_id, $clinic_id, $dc_id);
			return response()->json($slots);
	    } else {
    		return response()->json(['error'    =>  'Forbidden'], 403);
	    }
    }
}

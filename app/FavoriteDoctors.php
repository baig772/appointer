<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FavoriteDoctors extends Model
{
    //
    protected $table    =   'favourite_doctors';
    use SoftDeletes;
}

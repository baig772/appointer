<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware'  =>  'api'], function () {
    Route::group(['prefix'  =>  'v1'], function () {

        Route::get('test_lang', 'API\v1\TestController@index');
        Route::post('login', 'API\v1\AuthController@login');
        Route::post('verify-otp', 'API\v1\AuthController@verifyOtp');
        Route::post('resend-otp', 'API\v1\AuthController@resendOtp');
    });
});

Route::group(['middleware'  =>  'auth:api'], function () {
    Route::group(['prefix'  =>  'v1'], function () {
        Route::get('get-doctor-list', 'API\v1\DoctorsController@index');
        Route::post('favorite-doctor', 'API\v1\DoctorsController@favorite');
        Route::get('get-favorite-doctor', 'API\v1\DoctorsController@favoriteDoctors');

        Route::get('get-contact-list', 'API\v1\ContactListController@index');
        Route::post('add-contact', 'API\v1\ContactListController@store');

        Route::post('search-doctor', 'API\v1\DoctorsController@search');

        Route::get('get-doctor-detail/{id}', 'API\v1\DoctorsController@show');

        Route::get('pick-a-date/{doc_id}/{clinic_id}/{dc_id}', 'API\v1\TimeslotsController@getTimeslots');

        Route::post('book-appointment', 'API\v1\AppointmentsController@confirmAppointment');
    });
});
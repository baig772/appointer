<?php

use Illuminate\Database\Seeder;

class DoctorClinicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doctor_clinics')->insert([
            [
                'doctor_id' =>  1,
                'clinic_id' =>  1
            ],
            [
                'doctor_id' =>  1,
                'clinic_id' =>  2
            ],
            [
                'doctor_id' =>  1,
                'clinic_id' =>  3
            ]
        ]);
    }
}

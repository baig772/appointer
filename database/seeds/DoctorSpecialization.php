<?php

use Illuminate\Database\Seeder;

class DoctorSpecialization extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doctor_specialization')->insert([
            [
                'doctor_id' =>  1,
                'specialization_id' =>  1,
            ],
            [
                'doctor_id' =>  1,
                'specialization_id' =>  2,
            ],
            [
                'doctor_id' =>  2,
                'specialization_id' =>  3,
            ],
            [
                'doctor_id' =>  3,
                'specialization_id' =>  2,

            ],
            [
                'doctor_id' =>  3,
                'specialization_id' =>  3,

            ]
        ]);
    }
}

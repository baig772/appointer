<?php

use Illuminate\Database\Seeder;

class DoctorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doctors')->insert([
            [
                'first_name'    =>  'Ahmad',
                'last_name'     =>  'Baig',
                'phone_number'  =>  '923325958055',
                'qualification' =>  'MBBS FCPS',
                'status'        =>  1,
                'email'         =>  'ahmadbaig.qt@gmail.com'
            ],
            [
                'first_name'    =>  'Umair',
                'last_name'     =>  'Ahmad',
                'phone_number'  =>  '923325958057',
                'qualification' =>  'MBBS FCPS',
                'status'        =>  1,
                'email'         =>  'umair.qt@gmail.com'
            ],
            [
                'first_name'    =>  'Khalid',
                'last_name'     =>  'Mughal',
                'phone_number'  =>  '923325958056',
                'qualification' =>  'MBBS FCPS',
                'status'        =>  1,
                'email'         =>  'mkhalid.qt@gmail.com'
            ]
        ]);
    }
}

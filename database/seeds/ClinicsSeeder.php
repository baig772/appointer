<?php

use Illuminate\Database\Seeder;

class ClinicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clinics')->insert([
            [
                'name'  =>  'Maryam Memorial',
                'phone_number'  =>  '123456789'
            ],
            [
                'name'  =>  'Shifa International',
                'phone_number'  =>  '321654987'
            ],
            [
                'name'  =>  'Cosmosurge',
                'phone_number'  =>  '123456789'
            ],
            [
                'name'  =>  'Valley Clinic',
                'phone_number'  =>  '12345678'
            ]
        ]);
    }
}

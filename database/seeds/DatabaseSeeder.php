<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(DoctorsSeeder::class);
        $this->command->info('Doctors Table seeded');

        $this->call(SpecializationsSeeder::class);
        $this->command->info('Specializations Seeded');

        $this->call(DoctorSpecialization::class);
        $this->command->info('Specializations for doctors entered');

        $this->call(ClinicsSeeder::class);
        $this->command->info('clinics table seeded');

        $this->call(DoctorClinicSeeder::class);
        $this->command->info('Doctor Clinics data seeded');
    }
}

<?php

use Illuminate\Database\Seeder;

class SpecializationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('specializations')->insert([
            [

                'name' =>  'Dentist',
                'created_at'    =>  \Carbon\Carbon::now()
            ],
            [
                'name' =>  'Psychiatrist',
                'created_at'    =>  \Carbon\Carbon::now()
            ],
            [

                'name' =>  'Gynecologist',
                'created_at'    =>  \Carbon\Carbon::now()
            ],
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Timeslots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timeslots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doctor_clinic_id')->unsigned();
            $table->integer('average_patients');
            $table->string('average_time_each_patient');
            $table->time('start_time');
            $table->time('end_time');
            $table->tinyInteger('day');
            $table->enum('shift', ['morning', 'evening', 'night'])->default('morning');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('doctor_clinic_id')->references('id')->on('doctor_clinics');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeslots');
    }
}

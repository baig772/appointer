<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TimeslotSessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timeslot_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('timeslot_id')->unsigned();
            $table->string('timeslot_session')->nullable();
            $table->date('session_date');
            $table->time('session_time');
            $table->enum('session_status', ['booked', 'confirmed', 'checked', 'skipped', 'cancelled']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('timeslot_id')->references('id')->on('timeslots');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeslot_sessions');
    }
}
